:icons: font
// fa (Font-Awesome), octicon, fi (Foundation Icons), pf (Payment font)
:icon-set: fa
:source-highlighter: rouge
= BOOK TITLE
:author: Philippe Charrière © Bots.Garden
:toc:
:toc-placement: preamble
:toc-title: 🚀 Table des matières 
:toclevels: 4
:sectnums:
:chapter-label:
:doctype: book
:title-logo-image: images/logo-page.png

include::00-introduction.adoc[]

include::01-title.adoc[]

include::99-sample.adoc[]





